import { useState, useEffect }  from 'react'
import { Link } from 'react-router-dom'

export default function Home() {
    const [filmes, setFilmes] = useState([])

    useEffect(() => {
        
        fetch('https://sujeitoprogramador.com/r-api/?api=filmes/')
        .then(res => res.json())
        .then(resposta => {setFilmes(resposta)})
    }, [])

    return (
        <div className="container">
            <ul>
                {filmes.map(filme => {
                    return (
                        <li key={filme.id}>
                            <h2>{filme.nome}</h2>
                            <img src={filme.foto} alt={filme.foto} />
                            <Link to={`/filme/${filme.id}`}>Acessar</Link>
                        </li>
                    )
                    
                })}
            </ul>
        </div>
    )
}