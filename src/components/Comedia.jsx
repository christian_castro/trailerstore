import {useState, useEffect} from 'react'


export default function Comedia() {
    const [comedia, setComedia] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        fetch(`https://sujeitoprogramador.com/r-api/?api=filmes/546`)
        .then(response => response.json())
        .then(json => {setComedia(json)})
        setLoading(false) 
    }, [])

    if(loading) {
        return <h1>Carregando o conteúdo...</h1>
    }
    
    return (
        <div className="filme-listado">
            <h2>{comedia.nome}</h2>
            <img src={comedia.foto} alt={comedia.foto} />
            
            <p>{comedia.sinopse}</p>
            <button>
                <a target="_blank" rel="noreferrer" href={`https://youtube.com/results?search_query=${comedia.nome} trailer`}>Trailer</a>
            </button>
        </div>
    )
}