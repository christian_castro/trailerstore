import { Link } from 'react-router-dom'

export default function Header() {
    return (
        <header className="links-header">
            <h1>TrailerStore</h1>
            <Link to={"/"}>Início</Link>
            <Link to={"/comedia"}>Trailer Comédia</Link>
            <Link to={"/favoritos"}>Trailers Salvos</Link>
        </header>
    )
}