import Rotas from './Rotas'
import React from 'react'

import '../index.css'

import 'react-toastify/dist/ReactToastify.css'
import {ToastContainer} from 'react-toastify'

export default function App() {
  return (
    <div>
        <Rotas />
        <ToastContainer autoClose={3000} />
    </div>
  );
}

