import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify';

export default function Favoritos() {

    const [filme, setFilmes] = useState([])

    useEffect(() => {
        const pegarListas = localStorage.getItem('filme')
        setFilmes(JSON.parse(pegarListas) || [])
    }, [])


    function handleDelet(id) {
        let filmesFiltrados = filme.filter(filmes => {
            return (filmes.id !== id)
        })

        setFilmes(filmesFiltrados)
        localStorage.setItem('filme', JSON.stringify(filmesFiltrados))
        toast.success('Trailer excluído com sucesso')
    }


    return (
        <div className="meus-filmes">
                <h2 className="favoritos-subtitulo">Seus Trailers salvos estão aqui.</h2>

                {filme.length === 0 && <h4 className="favoritos-length-vazio">Poxa...Você não Adicionou nenhum filme aos favoritos ainda :(</h4>}
            <ul>
                {filme.map(filmes => {
                    return (
                        <li key={filmes.id}>
                                <h3>{filmes.nome}</h3>
                            <div className="favoritos-funcoes">
                                <Link to={`/filme/${filmes.id}`}>Ver detalhes</Link> 
                                <button onClick={() => handleDelet(filmes.id)}> Excluir</button>
                            </div>
                        </li>
                    )
                })}
            </ul>
        </div>
    )
}