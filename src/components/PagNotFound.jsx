import React from 'react'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify';

export default function PageNotFound() {
    toast.info('Página inválida, os links abaixo vão ajudar você a se guiar melhor')
    return (
        
        <div className="pageNotFound">
            
            <h1 className="page-titulo">Parece que 
            essa página não existe :(</h1>
            <h3>Links que podem ajudar:</h3> <br />
            <Link to={"/"}>Home </Link>
            <Link to={"/favoritos"}>Salvos</Link>
        </div>
    )
}
