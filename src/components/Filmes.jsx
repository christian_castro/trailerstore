import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { toast } from 'react-toastify';

export default function Filmes() {
    const { id } = useParams()
    const [filme, setFilme] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        fetch(`https://sujeitoprogramador.com/r-api/?api=filmes/${id}`)
        .then(res => res.json())
        .then(json => {
            setFilme(json)
            setLoading(false)        
        })
    }, [id])


    function handleSaved() {
        const listasStorage = localStorage.getItem('filme')

        let receberListas = JSON.parse(listasStorage) || []

        const jaSalvo = receberListas.some(filmes => filmes.id === filme.id)

        if(jaSalvo) {
            toast.error('Você já possui esse Trailer em seus favoritos, por favor selecione outro')
            return;
        }

        receberListas.push(filme)
        localStorage.setItem('filme', JSON.stringify(receberListas))
        toast.success('Trailer salvo com sucesso')
    }

    if(loading) {
        return <h1>Carregando o conteúdo...</h1>
    }

    return (
        <div className="filme-listado">
            <h2>{filme.nome}</h2>

            <img src={filme.foto} alt={filme.foto} />

            <h3>Sinopse</h3>


            <h4>{filme.sinopse}</h4>
            <div className="filme-listado-botoes">
                <button onClick={handleSaved}>Salvar</button>
                <button><a target="_blank" rel="noreferrer" href ={`https://youtube.com/results?search_query=${filme.nome} trailer`}>Trailer</a>
                </button>
            </div>
        </div>
    )
}