import React from 'react'
import Home from './Home'
import '../index.css'
import Header from './Header'
import Filmes from './Filmes'
import Favoritos from './Favoritos'
import PagNotFound from './PagNotFound'
import Comedia from './Comedia'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'

const Routes = () => {
    return (
    <Router>
        <Header />
        <div>
            <Switch>
                <Route exact path="/"> <Home /> </Route>
                <Route exact path="/filme/:id"> <Filmes /> </Route>
                <Route exact path="/favoritos"> <Favoritos /> </Route>
                <Route exact path="/comedia"> <Comedia /></Route>
                <Route  path="*"> <PagNotFound /> </Route>
            </Switch>
        </div>
    </Router>
    )
}

export default Routes